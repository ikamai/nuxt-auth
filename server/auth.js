const bodyParser = require('body-parser')
const session = require('express-session')

module.exports = function(app) {
  app.use(bodyParser.json())
  app.post('/api/login', (req, res, next) => {
    if (req.body.username === 'admin' && req.body.password === 'kopet1234') {
      session.authUser = { username: 'admin' }
      return res.json({
        username: 'admin',
        token: 'kovet872483'
      })
    }
    res.json({
      msg: 'login failed'
    })
  })

  app.get('/api/login', (req, res, next) => {
    res.json({
      tiak: session.authUser
    })
  })

  app.post('/api/logout', (req, res, next) => {
    delete session.authUser
    res.json({
      msg: 'logout ok'
    })
  })
}
