import consola from 'consola'
export default function({ store, redirect }) {
  consola.info('xxx')
  // Jika user tidak terautentikasi
  if (!store.state.auth.loggedIn) {
    return redirect('/login')
  }
}
