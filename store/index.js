// import consola from 'consola'
import axios from 'axios'
// consola.info('x-x')
export const state = () => ({
  auth: {},
  token: null
})

export const mutations = {
  setLoggedIn(state, { loggedIn, username }) {
    state.auth.loggedIn = loggedIn
    state.auth.user = username
  },
  setToken(state, token) {
    state.token = token
  }
}

export const actions = {
  // nuxtServerInit is called by Nuxt.js before server-rendering every page
  nuxtServerInit({ commit }, { req }) {
    // get login
    const t = this.$auth.$storage.getCookie('auth')
    try {
      if (t.loggedIn === true) {
        commit('setLoggedIn', {
          loggedIn: true,
          username: t.user
        })
      }

      // get token
      const tkn = this.$auth.$storage.getCookie('token')
      if (tkn) {
        commit('setToken', tkn)
      }
    } catch (error) {}
  },
  async login({ commit }, { username, password }) {
    try {
      const { data } = await axios.post('/api/login', {
        username,
        password
      })
      if (data) {
        if (data.username === username) {
          // set stor
          this.$auth.$storage.setCookie(
            'auth',
            {
              user: username,
              loggedIn: true
            },
            true
          )
          // set token
          this.$auth.$storage.setCookie('token', data.token, false)

          commit('setLoggedIn', {
            loggedIn: true,
            username
          })
          commit('setToken', data.token)
        }
      }
    } catch (error) {
      // error here
    }
  },

  async logout({ commit }) {
    this.$auth.$storage.setCookie('auth', false)
    this.$auth.$storage.setCookie('token', false)
    await axios.post('/api/logout')
    commit('setLoggedIn', {
      loggedIn: false,
      username: null
    })
    commit('setToken', null)
  }
}
